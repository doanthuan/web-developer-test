registerTest ('Contact page test', {
    setup: function() {

    },
    load:function() {
        this
            .waitForPageLoadAfter(function($) {
                $('#contact-us-page').submit();
            })
            .test("The form cannot be submitted if any of the required fields are empty?", function ($) {
                if (!$(".alert-danger").length) {
                    ok(true, "OK");
                } else {
                    ok(false, $(".alert-danger").text());
                }
            })
            .test("The form cannot be submitted if the enquiry type is 'Product complaint' " +
                "and any of the following fields are empty: product name, product size, use-by date or batch code.?", function ($) {
                var enquiry_type = $('#enquiry_type').val();
                if(enquiry_type == 'Product complaint')
                {
                    if (!$(".alert-danger").length) {
                        ok(true, "OK");
                    } else {
                        ok(false, $(".alert-danger").text());
                    }
                }
                ok(false, "Could not validate if previous test fail");
            });
    }
});
