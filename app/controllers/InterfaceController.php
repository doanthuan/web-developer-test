<?php

class InterfaceController extends BaseController
{

    /**
     * Shows the interface page
     *
     * @return mixed
     */
    public function showInterfacePage() {
        return View::make('secure.interface');
    }
} 