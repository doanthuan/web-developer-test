@extends('layouts.blank')

@section('content')

    <div class="ma-sign-in-wrapper">
        <div class="row">
            <div class="small-10 medium-8 small-offset-1 medium-offset-2 columns text-center">
                {{ HTML::image('img/logo-light.png', 'Please sign in', array('class' => 'ma-admin-logo')) }}
            </div>
        </div>
        <div class="row">
            <div class="small-8 medium-6 small-offset-2 medium-offset-3 columns">
                <h3>Please Sign In</h3>
                <br>
                {{ Form::open($formOptions) }}
                    @if(Session::get('signOutMessage'))
                        <div data-alert class="alert-box secondary radius">
                            {{ Session::get('signOutMessage') }}
                            <a href="#" class="close">&times;</a>
                        </div>
                    @endif
                    @if(Session::get('errorMessage'))
                        <div data-alert class="alert-box alert radius">
                            {{ Session::get('errorMessage') }}
                            <a href="#" class="close">&times;</a>
                        </div>
                    @endif
                    <label>
                        Username
                        {{ Form::text('username', Input::old('username'), array('placeholder' => 'Enter Username')) }}
                    </label>
                    <label>
                        Password
                        {{ Form::password('password', array('placeholder' => 'Enter Password')) }}
                    </label>
                    {{ Form::submit('Sign In', array('class' => 'button right')) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop
