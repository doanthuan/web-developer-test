@extends('layouts.public')

@section('content')
<div class="container">

    <div class="row" id="message">
        <div class="col-md-12">
            @if( is_object($errors) && $errors->all() )
            <div class="alert alert-danger well-sm alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                @foreach ($errors->all('<p>:message</p>') as $msg)
                {{ $msg }}
                @endforeach
            </div>
            @endif

            @if( isset($message) && !empty($message) )
            <div class="alert alert-success well-sm alert-dismissible">
                {{ $message }}
            </div>
            @endif
        </div>
    </div>

{{ Form::open(array('method' => 'post', 'class' => 'form-horizontal', 'id' => 'contact-us-page')) }}
    <div class="form-group">
        {{ Form::label('fname', 'First name', array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            {{ Form::text('fname', Input::old('fname'), array('class' => 'form-control', 'required')) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('sname', 'Surname', array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            {{ Form::text('sname', Input::old('sname'), array('class' => 'form-control', 'required')) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('email', 'Email address', array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            {{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'required')) }}
        </div>
    </div>

    <div class="form-group">
        {{Form::label('daytime_contact_number', 'Daytime Contact Number', array('class' => 'col-sm-2 control-label'))}}
        <div class="col-sm-10">
            {{Form::text('daytime_contact_number', Input::old('daytime_contact_number'), array('class' => 'form-control', 'required'))}}
        </div>
    </div>
    <br/><br/>
    <div class="form-group">
    {{Form::label('address', 'Address', array('class' => 'col-sm-2 control-label'))}}
        <div class="col-sm-10">
            {{Form::text('address', Input::old('address'), array('class' => 'form-control', 'required'))}}
        </div>
    </div>

    <div class="form-group">
        {{Form::label('suburb', 'Suburb', array('class' => 'col-sm-2 control-label'))}}
        <div class="col-sm-10">
            {{Form::text('suburb', Input::old('suburb'), array('class' => 'form-control','required'))}}
        </div>
    </div>

    <div class="form-group">
        {{Form::label('state', 'State', array('class' => 'col-sm-2 control-label'))}}
        <div class="col-sm-10">
            {{Form::select('state', array('ACT' => 'Australian Capital Territory', 'NSW' => 'New South Wales', 'NT' => 'Northern Territory', 'QLD' => 'Queensland', 'SA' => 'South Australia', 'TAS' => 'Tasmania', 'VIC' => 'Victoria', 'WA' => 'Western Australia'), Input::old('state'), array('class' => 'form-control', 'required') ) }}
        </div>
    </div>

    <div class="form-group">
        {{Form::label('postcode', 'Postcode', array('class' => 'col-sm-2 control-label'))}}
        <div class="col-sm-10">
            {{Form::text('postcode', Input::old('postcode'), array('class' => 'form-control', 'required'))}}
        </div>
    </div>
    <br/><br/>
    <div class="form-group">
        {{Form::label('enquiry_type', 'Enquiry Type', array('class' => 'col-sm-2 control-label'))}}
        <div class="col-sm-10">
            {{Form::select('enquiry_type', array('General enquiry' => 'General enquiry', 'Product feedback or enquiry' => 'Product feedback or enquiry', 'Product complaint' => 'Product complaint'), Input::old('enquiry_type'), array('class' => 'form-control', 'required'))}}
        </div>
    </div>

    <div class="form-group">
        {{Form::label('product_name', 'Product Name', array('class' => 'col-sm-2 control-label'))}}
        <div class="col-sm-10">
            {{Form::text('product_name', Input::old('product_name'), array('class' => 'form-control'))}}
        </div>
    </div>

    <div class="form-group">
        {{Form::label('product_size', 'Product Size', array('class' => 'col-sm-2 control-label'))}}
        <div class="col-sm-10">
            {{Form::text('product_size', Input::old('product_size'), array('class' => 'form-control'))}}
        </div>
    </div>

    <div class="form-group">
        {{Form::label('use_by_date', 'Use-by Date', array('class' => 'col-sm-2 control-label'))}}
        <div class="col-sm-10">
            {{Form::text('use_by_date', Input::old('use_by_date'), array('class' => 'form-control'))}}
        </div>
    </div>

    <div class="form-group">
        {{Form::label('batch_code', 'Batch Code', array('class' => 'col-sm-2 control-label'))}}
        <div class="col-sm-10">
            {{Form::text('batch_code', Input::old('batch_code'), array('class' => 'form-control'))}}
        </div>
    </div>

    <div class="form-group">
        {{Form::label('enquiry', 'Enquiry', array('class' => 'col-sm-2 control-label'))}}
        <div class="col-sm-10">
            {{Form::textarea('enquiry', Input::old('enquiry'), array('class' => 'form-control'))}}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10 text-right">
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </div>
{{ Form::close() }}

</div>

<style>
    .glyphicon-asterisk{
        color: #d9534f;
    }
</style>
<script>
    window.onload = function(){
        jQuery('.required')              .closest(".form-group").find("label").append("<i class='glyphicon-asterisk'></i>");
        jQuery('[required="required"]')  .closest(".form-group").find("label").append("<i class='glyphicon-asterisk'></i>");
    };
</script>
@stop