@extends('layouts._wrapper')

{{-- Keep in single line please --}}
@section('bodyClass') public @stop

@section('layout')
    @yield('content')
@stop