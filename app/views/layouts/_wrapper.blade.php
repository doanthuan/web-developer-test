<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <?php
    /**
     * Meta title: From lang/en/:view.php via controller
     *
     * Title is suffixed with "Many Amigos" which can be overwritten via $metaTitleSuffix
     */
    ?>
    <title>
        @if (isset($metaTitle))
            {{ $metaTitle }} |
        @endif
        @if (isset($metaTitleSuffix))
            {{ $metaTitleSuffix }}
        @else
            Many Amigos
        @endif
    </title>

    <?php
    /**
     * Meta tags: From lang/en/:view.php via controller
     */
    ?>
    @if (isset($metaTitle) && !is_null($metaTitle))
        <meta name="title" content="{{ $metaTitle }}" />
    @endif
    @if (isset($metaDescription) && !is_null($metaDescription))
        <meta name="description" content="{{ $metaDescription }}" />
    @endif
    @if (isset($metaKeywords) && !is_null($metaKeywords))
        <meta name="keywords" content="{{ $metaKeywords }}" />
    @endif
    <?php
    /**
     * Og tags: From lang/en/:view.php via controller
     *
     * The Open Graph protocol enables any web page to become a rich object in a social graph. http://ogp.me/
     */
    ?>
    @if (isset($ogTitle) && !is_null($ogTitle))
        <meta property="og:title" content="{{ $ogTitle }}" />
    @endif
    @if (isset($ogDescription) && !is_null($ogDescription))
        <meta property="og:description" content="{{ $ogDescription }}" />
    @endif
    @if (isset($ogImage) && !is_null($ogImage))
        <meta property="og:image" content="{{ url('/img/' . $ogImage) }}" />
    @endif

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{ HTML::style('assets/bootstrap/css/bootstrap.min.css') }}

    @if (App::environment() === 'local')
        {{ HTML::style('css/styles.css') }}
    @else
        {{ HTML::style('css/styles.min.css') }}
    @endif



</head>
<body data-ng-app="manyAmigosClient" class="@yield('bodyClass')">

<!--[if lt IE 9]>
<p class="browsehappy">
    You are using an <strong>outdated</strong> browser.
    Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</p>
<![endif]-->

@yield('layout')

@if (App::environment() === 'local')
    <?php // Libraries ?>
    {{ HTML::script('assets/bower_components/modernizr/modernizr.js') }}
    {{ HTML::script('assets/bower_components/jquery/dist/jquery.js') }}
    {{ HTML::script('assets/bower_components/fastclick/lib/fastclick.js') }}
    {{ HTML::script('assets/bower_components/foundation/js/foundation.js') }}
    {{ HTML::script('assets/bower_components/lodash/dist/lodash.js') }}
    {{ HTML::script('assets/bower_components/angular/angular.js') }}
<?php // Main app ?>
    {{ HTML::script('assets/js/app.js') }}
<?php // Controllers ?>
    {{ HTML::script('assets/js/controllers/MainController.js') }}
    {{ HTML::script('assets/js/controllers/MainMenuController.js') }}
    {{ HTML::script('assets/js/controllers/SignInController.js') }}
<?php // Providers ?>
    {{ HTML::script('assets/js/providers/UserManager.js') }}
@else
    {{ HTML::script('js/vendor.min.js') }}
    {{ HTML::script('js/scripts.min.js') }}
@endif

{{ HTML::script('assets/bootstrap/js/bootstrap.min.js') }}

@include('partials.google-analytics')

</body>
</html>