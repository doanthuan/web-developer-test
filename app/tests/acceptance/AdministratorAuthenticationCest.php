<?php

use \WebGuy;

class AdministratorAuthenticationCest
{
    /**
     * Attempts to log in as an administrator and checks if we make it through
     */
    public function checkAdministratorCanSignIn(WebGuy $I)
    {
        $I->wantTo('log in as an administrator user');
        $I->amOnPage('/ma-sign-in');
        $I->fillField('username', 'adminAmigo');
        $I->fillField('password', 'S3cur34cc355!');
        $I->click('Sign In');
        $I->see('Back to Site');
        $I->seeInCurrentUrl('/ma-admin');
    }

    /**
     * Attempts to log out
     */
    public function checkAdministratorCanSignOut(WebGuy $I)
    {
        $I->wantTo('log out');
        $I->click('Logout');
        $I->see('You have signed out of the Site Administration');
        $I->seeInCurrentUrl('/');
    }

    /**
     * Attempts to log into Laravel Administrator as an author user, we should expect a failure
     */
    public function checkAuthorCannotSignIn(WebGuy $I)
    {
        $I->wantTo('login as an author but fail');
        $I->fillField('username', 'authorAmigo');
        $I->fillField('password', 'S3cur34cc355!');
        $I->click('Sign In');
        $I->see('Username or password incorrect');
        $I->seeInCurrentUrl('/');
    }

}